﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightCodePanel : MonoBehaviour {

    public GameObject m_LightPrefab;
    public Vector3 m_Offset = new Vector3(0.0f, -1.0f, 0.0f);

    private Vector3 m_NextPosition;
    private List<Color> m_ColorOrder;
    private List<GameObject> m_LightList;


	// Use this for initialization
	void Start () {
        m_NextPosition = transform.position;

        SpawnLights();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SpawnLights()
    {
        m_LightList = new List<GameObject>();
        for (int i = 0; i < m_ColorOrder.Count; i++)
        {
            GameObject NewDoorLight = GameObject.Instantiate(m_LightPrefab, m_NextPosition, transform.rotation, transform);

            if (NewDoorLight)
            {
                Light DoorLight = NewDoorLight.GetComponentInChildren<Light>();
                if (DoorLight)
                {
                    DoorLight.color = m_ColorOrder[i];
                }
            }

            m_NextPosition += m_Offset;
        }
    }

    public void AddLight(Color LightColor)
    {
        if (m_ColorOrder == null)
        {
            m_ColorOrder = new List<Color>();
        }

        m_ColorOrder.Add(LightColor);
    }
}
