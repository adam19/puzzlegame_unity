﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class InteractionComponent : MonoBehaviour {

    public Text m_HUDText;
    public float m_MaxInteractionDistance = 1.5f;

    private Camera m_Camera;

    // Use this for initialization
    void Start () {
        m_HUDText.text = "";
        m_Camera = GetComponentInChildren<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        m_HUDText.text = "";

        RaycastHit HitInfo;
        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out HitInfo, m_MaxInteractionDistance))
        {
            InteractableInterface NearbyInterface = HitInfo.collider.GetComponentInParent<InteractableInterface>();
            
            if (NearbyInterface)
            {
                m_HUDText.text = NearbyInterface.GetInteractionMessage(gameObject);

                if (Input.GetKeyDown(KeyCode.Mouse0))
                {                    
                    NearbyInterface.ActivateInterface(gameObject);
                }
            }
        }
    }

    public void DisplayInteractionMessage(InteractableInterface Interactable)
    {
        m_HUDText.text = "";

        if (Interactable)
        {
            m_HUDText.text = Interactable.UIText;
        }
    }
}
