﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPanel : MonoBehaviour {

    public LightCodePanel m_LightCodePanel;
    public int m_NumLights = 4;
    public Door m_DoorToUnlock;
    public GameObject m_KeyToDrop;
    public AudioClip m_SuccessSound;
    public AudioClip m_ErrorSound;

    
    private List<int> m_ActivationOrder;
    private int m_ActivationIndex = 0;
    private AudioSource m_AudioSource;

    private void Awake()
    {
        m_ActivationOrder = new List<int>(m_NumLights);
        
        for (int i = 0; i < m_NumLights; i++)
        {
            m_ActivationOrder.Add((int)Math.Floor(UnityEngine.Random.value * 2.9f));

            Color LightColor = Color.white;
            switch (m_ActivationOrder[i])
            {
                case 0: LightColor = Color.red; break;
                case 1: LightColor = Color.green; break;
                case 2: LightColor = Color.blue; break;
            }
            m_LightCodePanel.AddLight(LightColor);
        }
    }

    // Use this for initialization
    void Start () {
        m_AudioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SwitchActivated(PanelSwitch ActivatedSwitch)
    {
        if (ActivatedSwitch)
        {
            if (m_ActivationOrder[m_ActivationIndex] == ActivatedSwitch.m_SwitchIndex)
            {
                ++m_ActivationIndex;
                Debug.Log("Valid entry");
            }
            else
            {
                // Invalid entry, reset activation order
                m_ActivationIndex = 0;
                m_AudioSource.PlayOneShot(m_ErrorSound);

                Debug.Log("Invalid sequence. Resetting");
            }

            if (m_ActivationIndex == m_NumLights)
            {
                // Successful sequence entered
                m_AudioSource.PlayOneShot(m_SuccessSound);

                if (m_KeyToDrop)
                {
                    m_KeyToDrop.SetActive(true);
                }
            }
        }
    }
}
