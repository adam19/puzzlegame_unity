﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour {

    public AudioClip m_PickupSound;

    private List<InventoryItem> m_Items;
    private AudioSource m_AudioSource;

    // Use this for initialization
    void Start () {
        m_Items = new List<InventoryItem>();
        m_AudioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public bool AddItemToInventory(InventoryItem NewItem)
    {
        if (NewItem)
        {
            Debug.Log("Added " + NewItem.m_ItemName + " to inventory!");

            m_Items.Add(NewItem);
            m_AudioSource.PlayOneShot(m_PickupSound);
        }

        return true;
    }

    public bool HasItem(string ItemName)
    {
        foreach (InventoryItem Item in m_Items)
        {
            if (Item.m_ItemName == ItemName)
            {
                return true;
            }
        }

        return false;
    }
}
