﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : InteractableInterface
{    
    public enum EDoorState
    {
        Closed,
        Opening,
        Opened,
        Closing
    };
    private EDoorState m_DoorState = EDoorState.Closed;

    public float m_OpenedAngleInDegrees = 90.0f;
    public float m_TimeToOpenInSeconds = 1.0f;
    public Vector3 m_PivotPosition;
    public string m_KeyIdNeededToUnlock;
    public AudioClip m_UnlockSound;
    public AudioClip m_OpeningSound;

    private Quaternion m_ClosedQuat;
    private Quaternion m_OpenedQuat;
    private AudioSource m_AudioSource;
    private float m_MovementTime = 0.0f;
    private bool bIsUnlocked = false;

    // Use this for initialization
    void Start () {
        m_DoorState = EDoorState.Closed;
        
        m_ClosedQuat = Quaternion.LookRotation(transform.forward, transform.up);
        m_OpenedQuat = Quaternion.Euler(0, m_OpenedAngleInDegrees, 0);

        m_AudioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {

        if (m_DoorState == EDoorState.Opening || m_DoorState == EDoorState.Closing)
        {
            float t = m_MovementTime / m_TimeToOpenInSeconds;
            m_MovementTime += Time.deltaTime;
            
            if (m_DoorState == EDoorState.Opening)
            {
                if (t > 1.0f)
                {
                    m_DoorState = EDoorState.Opened;
                }
            }
            else if (m_DoorState == EDoorState.Closing)
            {
                t = 1.0f - t;

                if (t < 0.0f)
                {
                    m_DoorState = EDoorState.Closed;
                }
            }
            
            transform.rotation = Quaternion.Slerp(m_ClosedQuat, m_OpenedQuat, t);
        }
	}

    public void ToggleDoor()
    {
        if (!bIsUnlocked)
        {
            return;
        }

        switch (m_DoorState)
        {
            case EDoorState.Closed:
                m_DoorState = EDoorState.Opening;
                m_MovementTime = 0.0f;
                break;
            case EDoorState.Opened:
                m_DoorState = EDoorState.Closing;
                m_MovementTime = 0.0f;
                break;
        }

        m_AudioSource.PlayOneShot(m_OpeningSound);

        Debug.Log("Changed door state to " + m_DoorState);
    }

    public override string GetInteractionMessage(GameObject Interactor)
    {
        string Message = "";
        
        InventoryManager InvManager = Interactor.GetComponent<InventoryManager>();
        if (InvManager && !InvManager.HasItem(m_KeyIdNeededToUnlock))
        {
            return "You do not have the correct key";
        }

        if (!bIsUnlocked)
        {
            return "Unlock door?";
        }

        switch(m_DoorState)
        {
            case EDoorState.Closed:
                Message = "Open?";
                break;
            case EDoorState.Opened:
                Message = "Close?";
                break;
        }

        return Message;
    }

    public override void ActivateInterface(GameObject Interactor)
    {
        InventoryManager InvManager = Interactor.GetComponent<InventoryManager>();
        
        if (InvManager)
        {
            Debug.Log("Valid InvManager - Needed Key=" + m_KeyIdNeededToUnlock);
            if (InvManager.HasItem(m_KeyIdNeededToUnlock))
            {
                if (!bIsUnlocked)
                {
                    if (m_UnlockSound)
                    {
                        m_AudioSource.PlayOneShot(m_UnlockSound);
                    }

                    bIsUnlocked = true;
                }
                else
                {
                    ToggleDoor();
                }
            }
        }
    }
}
