﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets.Characters.FirstPerson;

public class FPSController : MonoBehaviour {

    public float m_WalkSpeed = 3f;
    public float m_TurnSpeed = 150.0f;
    public float m_GravityMultiplier = 9.81f;

    private CharacterController m_Controller;
    private MouseLook m_MouseLook;
    private Camera m_Camera;
    Vector3 m_MoveDir = Vector3.zero;
    Vector2 m_Input = Vector2.zero;

    // Use this for initialization
    void Start () {
        m_Controller = GetComponent<CharacterController>();
        m_Camera = GetComponentInChildren<Camera>();

        m_MouseLook = new MouseLook();
        m_MouseLook.Init(transform, m_Camera.transform);
	}

    private void FixedUpdate()
    {

    }

    // Update is called once per frame
    void Update () {

        m_Input.x = Input.GetAxis("Horizontal");
        m_Input.y = Input.GetAxis("Vertical");

        if (m_Input.sqrMagnitude > 1)
        {
            m_Input.Normalize();
        }

        Vector3 DesiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;
        m_MoveDir.x = DesiredMove.x * m_WalkSpeed;
        m_MoveDir.z = DesiredMove.z * m_WalkSpeed;

        if (!m_Controller.isGrounded)
        {
            m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.deltaTime;
        }
        
        m_Controller.Move(m_MoveDir * Time.deltaTime);

        m_MouseLook.UpdateCursorLock();

        m_MouseLook.LookRotation(transform, m_Camera.transform);

    }
}
