﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EndGameVolume : MonoBehaviour
{
    public Text m_HudGameEndText;
    public float m_FadeOutTime = 5.0f;
    public float m_DelayToFade = 2.0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        	
	}

    private void OnTriggerEnter(Collider other)
    {
        if (m_HudGameEndText)
        {
            m_HudGameEndText.gameObject.SetActive(true);
        }

        // Start fading out the player's camera
        ScreenOverlay Overlay = other.gameObject.GetComponentInChildren<ScreenOverlay>();
        if (Overlay)
        {
            Overlay.StartFadingOut(m_FadeOutTime, m_DelayToFade);
        }
    }
}
