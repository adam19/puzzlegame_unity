﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableInterface : MonoBehaviour {

    public struct Option
    {
        string Key;
        string Description;
    };
    
    public string UIText;
    public Option[] Options;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        InteractionComponent OtherInteractor = other.gameObject.GetComponent<InteractionComponent>();
        if (OtherInteractor)
        {
            Debug.Log("OnTriggerEnter:");
            OtherInteractor.DisplayInteractionMessage(this);
        }
    }

    public virtual string GetInteractionMessage(GameObject Interactor)
    {
        return UIText;
    }

    public virtual void ActivateInterface(GameObject Interactor)
    {

    }
}
