﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelSwitch : InteractableInterface
{
    public SwitchPanel m_ParentPanel;
    public AudioClip m_PressedSound;
    public int m_SwitchIndex = -1;

    private AudioSource m_AudioSouce;

    // Use this for initialization
    void Start () {
        m_AudioSouce = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public override string GetInteractionMessage(GameObject Interactor)
    {
        return "Push?";
    }

    public override void ActivateInterface(GameObject Interactor)
    {
        if (m_ParentPanel)
        {
            m_ParentPanel.SwitchActivated(this);
        }

        m_AudioSouce.PlayOneShot(m_PressedSound);
    }
}
