﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureBox : InteractableInterface
{
    public bool m_Explored = false;
    public string m_NotExploredMessage = "Examine contents?";
    public string m_ExploredMessage = "You have already explored this box";
    public InventoryItem m_Item;
    public AudioClip m_ExploringSound;

    private AudioSource m_AudioSource;

	// Use this for initialization
	void Start () {
        m_AudioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    
    public override string GetInteractionMessage(GameObject Interactor)
    {
        if (m_Explored)
        {
            return m_ExploredMessage;
        }

        return m_NotExploredMessage;
    }

    public override void ActivateInterface(GameObject Interactor)
    {
        base.ActivateInterface(Interactor);

        // Add item to the other interactors inventory
        if (Interactor)
        {
            InventoryManager InvManager = Interactor.GetComponent<InventoryManager>();
            if (InvManager)
            {
                if (m_Item)
                {
                    InvManager.AddItemToInventory(m_Item);
                    m_Item.gameObject.SetActive(false);
                }

                m_AudioSource.PlayOneShot(m_ExploringSound);
                m_Explored = true;
            }
        }
    }
}
